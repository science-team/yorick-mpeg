%define name yorick-mpeg
%define version 0.1
%define release gemini2008apr30

Summary: yorick mpeg production
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}-%{version}.tar.bz2
License: BSD
Group: Development/Languages
Packager: Francois Rigaut <frigaut@users.sourceforge.net>
Url: http://yorick.sourceforge.net
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Requires: yorick >= 2.1


%description
This is a standalone mpeg movie encoder for yorick, intended to
replace the mpeg encoder in the yorick-z package, which requires the
ffmpeg package. yorick-mpeg builds a stripped down version of
libavcodec.

%prep
%setup -q

%build
yorick -batch make.i
make clean
make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/lib/yorick/lib
mkdir -p $RPM_BUILD_ROOT/usr/lib/yorick/i0
mkdir -p $RPM_BUILD_ROOT/usr/lib/yorick/i
mkdir -p $RPM_BUILD_ROOT/usr/share/doc/%{name}
mkdir -p $RPM_BUILD_ROOT/usr/lib/yorick/packages/installed

install -m 755 mpeg.so $RPM_BUILD_ROOT/usr/lib/yorick/lib
install -m 644 mpeg.i $RPM_BUILD_ROOT/usr/lib/yorick/i0
install -m 644 mpgtest.i $RPM_BUILD_ROOT/usr/lib/yorick/i
install -m 644 README $RPM_BUILD_ROOT/usr/share/doc/%{name}
install -m 644 mpeg.info $RPM_BUILD_ROOT/usr/lib/yorick/packages/installed

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/usr/lib/yorick/lib
/usr/lib/yorick/i0
/usr/lib/yorick/i
/usr/share/doc/%{name}
/usr/lib/yorick/packages/installed/*


%changelog
* Fri Jan 11 2008 <frigaut@users.sourceforge.net>
- initial release
